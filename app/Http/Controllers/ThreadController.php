<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;
use App\User;
use App\Http\Requests\CreateThreadRequest;
use App\Http\Requests\UpdateThreadRequest;

use Illuminate\Support\Facades\Log;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('Showing user profile for user');

        $threads = new Thread();
        if ($request->has('order') && strlen($request->order)) {
            $threads = $threads->orderBy('created_at', $request->order);
        }

        if ($request->has('a_order') && strlen($request->a_order)) {
            $threads = $threads->orderBy('title', $request->a_order);
        }

        if ($request->has('users')) {
            $threads = $threads->whereIn('user_id', $request->users);
        }

        $threads = $threads->get();
        $users = User::all();

        return view('threads', ['threads' => $threads, 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateThreadRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateThreadRequest $request)
    {
        if (auth()->user()->threads()->count() == 5) {
            auth()->user()->lastThread()->delete();
        }
        auth()->user()->threads()->create($request->toArray());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        $thread->load(['replies']);
        return view('view_thread', ['thread' => $thread]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        return view('edit_thread', ['thread' => $thread]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdateThreadRequest  $request
     * @param  App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateThreadRequest $request, Thread $thread)
    {
        if ($thread->user->is(auth()->user())) {
            $thread->update($request->toArray());
            return back()->withStatus('Thread updated successfully!');
        }

        return back()->withError('Cannot update this thread!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        if ($thread->user->is(auth()->user()) || auth()->user()->isAdmin()) {
            if ($thread->delete()) {   
                return back()->withStatus('Thread deleted successfully!');
            }
        }
        return back()->withError('Cannot delete this thread!');
    }
}
