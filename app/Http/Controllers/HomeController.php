<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check() && auth()->user()->isAdmin()) {
            $threads = Thread::all();
            return view('threads', ['threads' => $threads]);
        }
        return view('home');
    }
}
