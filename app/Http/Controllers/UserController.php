<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile()
    {
    	$threads = auth()->user()->threads()->orderBy('created_at', 'desc')->get();

    	return view('profile', ['threads' => $threads]);
    }
}
