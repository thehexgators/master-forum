<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;
use Illuminate\Support\Facades\Log;

class ReplyController extends Controller
{
    public function store(Request $request, Thread $thread)
    {
    	$thread->replies()->create($request->toArray());

    	$thread->load(['user']);
    	Log::info('User ' . $thread->user->name . ' has received a new reply to thread id:' . $thread->id);

    	return redirect()->back();
    }
}
