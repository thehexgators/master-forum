<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The current user is admin
     */
    public function isAdmin()
    {
        return $this->role_id == 2;
    }

    /**
     * Retrieve last user thread
     */
    public function lastThread()
    {
        return $this->threads()->orderBy('created_at', 'asc')->limit(1)->first();
    }

    /**
     * An user has many threads
     */
    public function threads()
    {
        return $this->hasMany('App\Models\Thread');
    }
}
