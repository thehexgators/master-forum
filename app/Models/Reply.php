<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
    ];

    /**
     * A reply belongs to a thread 
     */
    public function thread()
    {
    	return $this->belongsTo('App\Models\Thread');
    }
}
