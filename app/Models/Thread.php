<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content'
    ];


    /**
     * A thread belongs to an user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * An user has many replies
     */
    public function replies()
    {
        return $this->hasMany('App\Models\Reply');
    }
}
