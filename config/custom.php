<?php

return [
	'user_roles' => [
		1 => 'user',
		2 => 'admin'
	]
];