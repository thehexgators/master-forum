<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'admin',
        	'email' => 'admin@admin.com',
        	'password' => Hash::make('secret'),
        	'remember_token' => null,
        	'role_id' => 2,
        	'created_at' => now()->toDateTimeString(),
        	'updated_at' => now()->toDateTimeString()
        ]);
    }
}
