<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// AUTH ROUTES
Route::middleware(['auth'])->group(function () {
	Route::get('/profile', 'UserController@profile')->name('profile.index');


	// storing a new thread
	Route::get('/threads', 'ThreadController@index')->name('threads.index');
	Route::post('/threads', 'ThreadController@store')->name('threads.store');
	Route::get('/threads/{thread}', 'ThreadController@show')->name('threads.show');
	Route::get('/threads/{thread}/edit', 'ThreadController@edit')->name('threads.edit');
	Route::delete('/threads/{thread}', 'ThreadController@destroy')->name('threads.destroy');
	Route::put('/threads/{thread}', 'ThreadController@update')->name('threads.update');


	// storing replies for threads
	Route::post('/threads/replies/{thread}', 'ReplyController@store')->name('replies.store');
});
