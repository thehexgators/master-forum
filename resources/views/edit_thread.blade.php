@extends('layouts.app')

@section('content')

   	<form class="form-horizontal" method="POST" action="{{ route('threads.update', $thread->id) }}">
		{{ csrf_field() }}
		{{ method_field('PUT') }}

	    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	        <label for="title" class="col-md-4 control-label">Title</label>

	        <div class="col-md-6">
	            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') ?? $thread->title }}" required autofocus>

	            @if ($errors->has('title'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('title') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
	        <label for="content" class="col-md-4 control-label">Content</label>

	        <div class="col-md-6">
	            <textarea id="content" type="text" class="form-control" name="content" required>{{ old('content') ?? $thread->content }}</textarea>

	            @if ($errors->has('content'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('content') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group">
	        <div class="col-md-6 col-md-offset-4">
	            <button type="submit" class="btn btn-primary">
	                Update thread
	            </button>
	        </div>
	    </div>
	</form>

@endsection
