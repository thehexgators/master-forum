@extends('layouts.app')

@section('content')
	<div>title: {{ $thread->title }}</div>
	<div>content: {{ $thread->content }}</div>

	@if ($thread->replies->count())
		<ul>
				@foreach ($thread->replies as $reply)
				<li>
					{{ $reply->message }}
				</li>
				@endforeach
		</ul>
	@endif

	<form method="post" action="{{ route('replies.store', $thread->id) }}">
		{{ csrf_field() }}
		<input type="text" name="message">
		<input type="submit" value="Reply">
	</form>
@endsection
