@extends('layouts.app')

@section('content')
	<div>Name: {{ auth()->user()->name }}</div>
	<div>Email: {{ auth()->user()->email }}</div>

	Create new thread:
	<form class="form-horizontal" method="POST" action="{{ route('threads.store') }}">
		{{ csrf_field() }}
	    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	        <label for="title" class="col-md-4 control-label">Title</label>

	        <div class="col-md-6">
	            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

	            @if ($errors->has('title'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('title') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
	        <label for="content" class="col-md-4 control-label">Content</label>

	        <div class="col-md-6">
	            <textarea id="content" type="text" class="form-control" name="content" required>{{ old('content') }}</textarea>

	            @if ($errors->has('content'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('content') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group">
	        <div class="col-md-6 col-md-offset-4">
	            <button type="submit" class="btn btn-primary">
	                Save thread
	            </button>
	        </div>
	    </div>
	</form>
		@foreach ($threads as $thread)
			<div class="row">
				<div class="col-md-8">
					{{ $thread->title }}
				</div>
				<div class="col-md-2">
					<a href="{{ route('threads.edit', $thread->id) }}">Edit</a>
				</div>
				<div class="col-md-2">
					<form method="POST" action="{{ route('threads.destroy', $thread->id) }}">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
				
						<input type="submit" value="Delete">
					</form>
				</div>
			</div>
		@endforeach
@endsection