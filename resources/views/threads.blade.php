@extends('layouts.app')

@section('content')
	@if (!auth()->user()->isAdmin())
		<form method="get" action="{{ route('threads.index') }}">
			<div class="row">
				<div class="col-md-12">
					<select name="users[]" multiple="multiple">
						@foreach ($users as $user)
							<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<label for="oder">Date order</label>
					<select id="order" name="order">
						<option value="">-</option>
						<option value="asc">asc</option>
						<option value="desc">desc</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<label for="a_oder">Alphabetical order</label>
					<select id="a_order" name="a_order">
						<option value="">-</option>
						<option value="asc">asc</option>
						<option value="desc">desc</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<input type="submit" value="Filter">
				</div>
			</div>
		</form>

		<hr><hr>
	@endif

   	@foreach ($threads as $key => $thread)
   		<div class="row">

			<div class="col-md-12">
				#{{ $key }} Title: {{ $thread->title }} ( {{ $thread->created_at }} )
			</div>
			<div class="col-md-8">
				Content: {{ substr($thread->content, 0, 75) }}...
			</div>
			@if (auth()->user()->isAdmin())
				<div class="col-md-4">
					<form method="POST" action="{{ route('threads.destroy', $thread->id) }}">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
				
						<input type="submit" value="Delete">
					</form>
				</div>
			@endif
		</div>
		<hr>			
   	@endforeach

@endsection
